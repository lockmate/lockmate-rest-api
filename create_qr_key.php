<?php

/*
 * Following code will create a new QR key row
 * All product details are read from HTTP Post Request
 */

header('Content-Type: application/json');

// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['studentName']) && isset($_POST['matricNo']) && isset($_POST['password'])) {

    $studentName = $_POST['studentName'];
    $matricNo = $_POST['matricNo'];
    $password = $_POST['password'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysql_query("INSERT INTO qr_keys(studentName, matricNo, password) VALUES('$studentName', '$matricNo', '$password')");

    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Student successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";

        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>