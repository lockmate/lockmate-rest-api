<?php

/*
 * Following code will list all the QR keys
 */

header('Content-Type: application/json');

// array for JSON response
$response = array();

// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

// get all products from products table
$result = mysql_query("SELECT * FROM qr_keys") or die(mysql_error());

// check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["qr_keys"] = array();

    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $qr_key = array();
        $qr_key["id"] = $row["id"];
        $qr_key["studentName"] = $row["studentName"];
        $qr_key["matricNo"] = $row["matricNo"];
        $qr_key["password"] = $row["password"];

        // push single qr_key into final response array
        array_push($response["qr_keys"], $qr_key);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No QR keys found";

    // echo no users JSON
    echo json_encode($response);
}
?>