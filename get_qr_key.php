<?php

/*
 * Following code will get single QR key details
 */

header('Content-Type: application/json');

// array for JSON response
$response = array();

// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

// check for post data
if (isset($_GET["id"])) {
    $id = $_GET['id'];

    // get a product from products table
    $result = mysql_query("SELECT *FROM qr_keys WHERE id = $id");

    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);

            $qr_key = array();
            $qr_key["id"] = $result["id"];
            $qr_key["studentName"] = $result["studentName"];
            $qr_key["matricNo"] = $result["matricNo"];
            $qr_key["password"] = $result["password"];

            // success
            $response["success"] = 1;

            // user node
            $response["qr_key"] = array();

            array_push($response["qr_key"], $qr_key);

            // echoing JSON response
            echo json_encode($response);
        } else {
            // no product found
            $response["success"] = 0;
            $response["message"] = "No QR key found";

            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No QR key found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>